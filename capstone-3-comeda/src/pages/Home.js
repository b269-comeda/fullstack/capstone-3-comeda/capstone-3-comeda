// import Banner from '../components/Banner';
import { Container, Row, Col, Button, Card, Carousel } from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function Home() {

	// const data = {
	// 	title: "Jimmy Buckets",
	// 	content: "Pocket-friendly. For Pockets Full",
	// 	destination: "/products",
	// 	label: "Bili na bes!"
	// }


	return (
		<>
    <section className="hero rounded-pill">
      <div className="hero-content">
        <h1 className="hero-title text-dark">Pocket-friendly. For Pockets Full*</h1>
        <p className="hero-subtitle"><br/></p>
        <Button id="hero-button" as={Link} to='/products'>Shop Now</Button>
      </div>
    </section>
		{/*<Banner data={data} />*/}
		 <Container fluid>
      <Row className="mt-5">
        <Col md={8}>
          <Carousel>
            <Carousel.Item>
              <img
                className="d-block w-100"
                src="https://cdn.shopify.com/s/files/1/0587/8179/4470/files/NAIL_ONYX_WEB_1080x.jpg?v=1677133125"
                alt="First slide"
              />
            </Carousel.Item>
            <Carousel.Item>
              <img
                className="d-block w-100"
                src="https://cdn.shopify.com/s/files/1/0587/8179/4470/files/NAIL_JADE_WEB_1080x.jpg?v=1677178518"
                alt="Second slide"
              />
            </Carousel.Item>
            <Carousel.Item>
              <img
                className="d-block w-100"
                src="https://cdn.shopify.com/s/files/1/0587/8179/4470/files/NAIL_PEACH_WEB_1080x.jpg?v=1677178532"
                alt="Third slide"
              />
            </Carousel.Item>
          </Carousel>
          <Carousel className="my-5">
            <Carousel.Item>
              <img
                className="d-block w-100"
                src="https://64.media.tumblr.com/a2dadfdb5de9b52b3f414d70f87e19ba/ff9eef8943554db9-65/s1280x1920/3a84d6132506ad46d6e01f58cef7d94e1098da75.jpg"
                alt="First slide"
              />
            </Carousel.Item>
            <Carousel.Item>
              <img
                className="d-block w-100"
                src="https://64.media.tumblr.com/588ffde4d70cc2ce8f27b138d5b2a2ec/ff9eef8943554db9-82/s1280x1920/b3401d34456081eb034841908820e267d1f33b3d.jpg"
                alt="Second slide"
              />
            </Carousel.Item>
            <Carousel.Item>
              <img
                className="d-block w-100"
                src="https://64.media.tumblr.com/10fa4ec9fd375d12f6d8a596270b57ce/b95d1b32ebde8c17-0e/s1280x1920/bf1e3ba2cb37972fbd30240c5fbd730615a96fa7.jpg"
                alt="Third slide"
              />
            </Carousel.Item>
          </Carousel>
        </Col>
        <Col md={4}>
          <Card className="mb-3">
            <Card.Img variant="top" src="https://cdn.shopify.com/s/files/1/0587/8179/4470/files/biketwoweb_1000x.jpg?v=1670637521" />
            <Card.Body>
              <Card.Title><b>le FLEUR* PETALERS</b></Card.Title>
              <Card.Text>
                <b>A short-sleeved knit polo shirt with a relaxed fit and vented hem, made from combed Italian cotton in a wavy pattern and featuring hand-stitched flower buttons.</b>
              </Card.Text>
              <Button id="product-btn" variant="primary" as={Link} to='/products'>Buy Now</Button>
            </Card.Body>
          </Card>
          <Card className="mb-3">
            <Card.Img variant="top" src="https://cdn.shopify.com/s/files/1/0587/8179/4470/files/DOS_web_1000x.jpg?v=1670865751" />
            <Card.Body>
              <Card.Title><b>FRENCH WALTZ</b></Card.Title>
              <Card.Text>
                <b>FRENCH WALTZ</b> is dozing off in the garden, using the sun as a towel to dry off the leftover beads of lake water. The smell of damp jasmine petals sails in the air, while hints of mandarin and magnolia amplify luminous sandalwood. Sweet yet floral, FRENCH WALTZ is a rose-musk made for everyone.
              </Card.Text>
              <Button id="product-btn" variant="primary" as={Link} to='/products'>Buy Now</Button>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
		</>
	)
}
